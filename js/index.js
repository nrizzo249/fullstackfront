$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 3000
    });

    $('#contacto').on('show.bs.modal', function(e) {
        console.log('El modal de contacto se está mostrando');

        $('#btnContactUs').removeClass('btn-outline-warning');
        $('#btnContactUs').addClass('btn-outline-secondary');
        $('#btnContactUs').prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function(e) {
        console.log('El modal de contacto se mostró');
    });

    $('#contacto').on('hide.bs.modal', function(e) {
        console.log('El modal de contacto se está ocultando');
    });

    $('#contacto').on('hidden.bs.modal', function(e) {
        console.log('El modal de contacto se ocultó');

        $('#btnContactUs').removeClass('btn-outline-secondary');
        $('#btnContactUs').addClass('btn-outline-warning');
        $('#btnContactUs').prop('disabled', false);
    });
});